package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class sigup extends AppCompatActivity {
TextView txt_sigin,validPass;
Button Bt_sigup;
ArrayList<acount> arrayList;
EditText name,pass,confimPass,mail;
String txtName,txtPass,txtConfrim,txtMail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sigup);
        validPass=(TextView)findViewById(R.id.text_confirmPass);
        txt_sigin=(TextView)findViewById(R.id.txtSignin);
        mail=(EditText) findViewById(R.id.inputEmail);
        name=(EditText)findViewById(R.id.inputName);
        confimPass=(EditText)findViewById(R.id.inputConfirmPass);
        Bt_sigup=(Button) findViewById(R.id.buttonSignup);
        pass=(EditText)findViewById(R.id.inputPasswprd);

        arrayList = new ArrayList<>();

        Bt_sigup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Bundle bundle=new Bundle();
                String pass1=pass.getText().toString().trim();
                String pass2=confimPass.getText().toString().trim();
                if(pass1.equals(pass2)){
                     txtName = name.getText().toString();
                     txtPass = pass.getText().toString().trim();
                     txtMail=mail.getText().toString();
                     acount ac1=new acount(txtName,txtPass,txtMail);
                    arrayList.add(ac1);
                    Toast.makeText(getApplicationContext(),"Đăng ký thành công",Toast.LENGTH_SHORT).show();
                }
                else
                    validPass.setText("Mật khẩu nhập lại không khớp");
            }
        });
        txt_sigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(sigup.this,MainActivity.class);
                i.putExtra("mail",arrayList.get(arrayList.size()-1).email);
                i.putExtra("pass",arrayList.get(arrayList.size()-1).pass);
                startActivity(i);

            }
        });
    }
}