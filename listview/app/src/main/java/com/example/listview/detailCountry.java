package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class detailCountry extends AppCompatActivity {
    TextView tenSV,msv;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_country);
        tenSV=(TextView)findViewById(R.id.tenSV);
        msv=(TextView)findViewById(R.id.MSV);
        img=(ImageView)findViewById(R.id.imgdetail);
        String ten=getIntent().getStringExtra("tenSV");
        String masv=getIntent().getStringExtra("MSV");

        tenSV.setText(ten);
        msv.setText(masv);
        img.setImageResource(R.drawable.a);

    }
}