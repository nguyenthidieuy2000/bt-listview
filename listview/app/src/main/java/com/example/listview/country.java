package com.example.listview;
public class country {
    public  String tenSV,MSV;
    public int img;
    public country(String tenSV, String MSV, int img) {
        this.tenSV = tenSV;
        this.MSV = MSV;
        this.img = img;
    }
    public String gettenSV() {
        return tenSV;
    }
    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }
    public String getMSV() {
        return MSV;
    }
    public void setMSV(String MSV) {
        this.MSV = MSV;
    }
    public int getImg() {
        return img;
    }
    public void setImg(int img) {
        this.img = img;
    }
}
