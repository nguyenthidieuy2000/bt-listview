package com.example.listview;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
public class listItem extends AppCompatActivity {
    ListView lv;
    ArrayList<country> arrayList;
    countryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);
        lv = (ListView) findViewById(R.id.lvSV);
        arrayList=new ArrayList<>();
        arrayList.add(new country("Nguyễn Thị Diệu Ý","1811505310256",R.drawable.a));
        arrayList.add(new country("Nguyễn Thị Hoa","1811505310256",R.drawable.b));
        arrayList.add(new country("Nguyễn Na","1811505310256",R.drawable.c));
        arrayList.add(new country("Nguyễn Hà","1811505310256",R.drawable.a));
        arrayList.add(new country("Nguyễn Lan","1811505310256",R.drawable.b));
        arrayList.add(new country("Nguyễn Mai","1811505310256",R.drawable.c));
        adapter = new countryAdapter(this,R.layout.dong_nuoc, arrayList);
          lv.setAdapter(adapter);
          lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                  ShowItem(position);}});
    }
    private void ShowItem(int position){
        country c= arrayList.get(position);
        Intent i = new Intent( this, detailCountry.class);
        i.putExtra("tenSV",c.gettenSV());
        i.putExtra("MSV",c.getMSV());
        startActivity(i);
    }
}