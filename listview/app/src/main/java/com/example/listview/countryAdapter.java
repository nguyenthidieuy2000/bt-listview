package com.example.listview;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;
public class countryAdapter extends BaseAdapter {
    Context context;
    int  layout;
    List<country> countries;
    public countryAdapter(Context context, int layout, List<country> countries) {
        this.context = context;
        this.layout = layout;
        this.countries = countries; }
    @Override
    public int getCount() {
        return countries.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(layout,null);
        TextView tvTenSV=(TextView)convertView.findViewById(R.id.tvTenSV);
        TextView tvMSV = (TextView)convertView.findViewById(R.id.tvMSV);
        ImageView imgco=(ImageView)convertView.findViewById(R.id.imageco);
         tvTenSV.setText(countries.get(position).tenSV);
         tvMSV.setText(countries.get(position).MSV);
         imgco.setImageResource(countries.get(position).img);
        return convertView; }
}
